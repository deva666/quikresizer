﻿/*
  QuikResizer 
  Copyright (C) 2013 Marko Devcic
   
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation <http://www.gnu.org/licenses/>.
  
  This application comes without WARRANTY WHATSOEVER!
 */

using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Media.Animation;
using System.Threading;
using System.Threading.Tasks;

namespace QuikResizer
{
    enum ImgEncoder : byte
    {
        Jpg,
        Bmp,
        Png,
        Default
    }

    public partial class MainWindow : Window
    {
        int _width;
        int _height;
        string _sufix;
        string _folderPath;
        object _locker = new object();
        ImgEncoder _encoder = ImgEncoder.Default;

        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += (s, e) => { ScaleLoaded(); };
        }

        #region Animations

        private void ScaleLoaded()
        {
            DoubleAnimation scaleX = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromSeconds(0.4)));
            DoubleAnimation scaleY = new DoubleAnimation(0.0, 1.0, new Duration(TimeSpan.FromSeconds(0.4)));

            scaleX.SetValue(Storyboard.TargetProperty, this);
            scaleY.SetValue(Storyboard.TargetProperty, this);

            Storyboard story = new Storyboard();
            Storyboard.SetTarget(scaleX, this);
            Storyboard.SetTarget(scaleY, this);
            Storyboard.SetTargetProperty(scaleX, new PropertyPath("RenderTransform.ScaleX"));
            Storyboard.SetTargetProperty(scaleY, new PropertyPath("RenderTransform.ScaleY"));

            this.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            story.Children.Add(scaleX);
            story.Children.Add(scaleY);
            story.Begin(this);
        }

        private void ScaleUnloaded()
        {
            DoubleAnimation scaleX = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromSeconds(0.4)));
            DoubleAnimation scaleY = new DoubleAnimation(1.0, 0.0, new Duration(TimeSpan.FromSeconds(0.4)));

            scaleX.SetValue(Storyboard.TargetProperty, this);
            scaleY.SetValue(Storyboard.TargetProperty, this);

            Storyboard story = new Storyboard();
            Storyboard.SetTarget(scaleX, this);
            Storyboard.SetTarget(scaleY, this);
            Storyboard.SetTargetProperty(scaleX, new PropertyPath("RenderTransform.ScaleX"));
            Storyboard.SetTargetProperty(scaleY, new PropertyPath("RenderTransform.ScaleY"));

            this.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            story.Children.Add(scaleX);
            story.Children.Add(scaleY);
            story.Completed += (s, e) => { this.Close(); };
            story.Begin(this);
        }

        #endregion

        #region ClassMethods

        private ImageFormat GetImageFormat(string path)
        {
            ImageFormat format;
            string ext = System.IO.Path.GetExtension(path);
            switch (ext.ToLower())
            {
                case ".jpg":
                    format = ImageFormat.Jpeg;
                    break;
                case ".jpeg":
                    format = ImageFormat.Jpeg;
                    break;
                case ".bmp":
                    format = ImageFormat.Bmp;
                    break;
                case ".png":
                    format = ImageFormat.Png;
                    break;
                case ".ico":
                    format = ImageFormat.Icon;
                    break;
                case ".tiff":
                    format = ImageFormat.Tiff;
                    break;
                case ".tif":
                    format = ImageFormat.Tiff;
                    break;
                case ".gif":
                    format = ImageFormat.Gif;
                    break;
                case ".exif":
                    format = ImageFormat.Exif;
                    break;
                default:
                    format = ImageFormat.Jpeg;
                    break;
            }
            return format;
        }

        private void ResizeImages()
        {
            try
            {
                lock (_locker)
                {
                    Dispatcher.Invoke(new Action(() => { pBar.Visibility = System.Windows.Visibility.Visible; }));
                    List<string> list = new List<string>();

                    foreach (var item in lbFiles.Items)
                    {
                        list.Add(item as string);
                    }

                    foreach (var item in list)
                    {
                        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(item);
                        bool cbChecked = false;
                        Dispatcher.Invoke(new Action(() => { cbChecked = cbAspectRatio.IsChecked.Value; }));
                        if (cbChecked)
                        {
                            int oldWidth = bmp.Width;
                            int oldHeight = bmp.Height;
                            double aspectRatio = (double)oldWidth / (double)oldHeight;
                            if (oldWidth >= oldHeight)
                            {
                                _height = Convert.ToInt32((double)_width / aspectRatio);
                            }
                            else
                            {
                                _width = Convert.ToInt32((double)_height * aspectRatio);
                            }
                        }
                        System.Drawing.Imaging.ImageFormat format;
                        string ext;
                        switch (_encoder)
                        {
                            case ImgEncoder.Jpg:
                                ext = ".jpg";
                                format = System.Drawing.Imaging.ImageFormat.Jpeg;
                                break;
                            case ImgEncoder.Bmp:
                                ext = ".bmp";
                                format = System.Drawing.Imaging.ImageFormat.Bmp;
                                break;
                            case ImgEncoder.Png:
                                ext = ".png";
                                format = System.Drawing.Imaging.ImageFormat.Png;
                                break;
                            case ImgEncoder.Default:
                                ext = System.IO.Path.GetExtension(item as string);
                                format = GetImageFormat(ext);
                                break;
                            default:
                                ext = ".jpg";
                                format = System.Drawing.Imaging.ImageFormat.Jpeg;
                                break;
                        }
                        string newName = System.IO.Path.GetFileNameWithoutExtension(item as string) + _sufix + ext;
                        string path;
                        if (string.IsNullOrEmpty(_folderPath))
                        {
                            path = System.IO.Path.GetDirectoryName(item) + "\\" + newName;
                        }
                        else
                        {
                            path = _folderPath + "\\" + newName;
                        }
                        System.Drawing.Bitmap newImg = new System.Drawing.Bitmap(bmp, new System.Drawing.Size(_width, _height));
                        newImg.Save(path, format);
                        Dispatcher.Invoke(new Action(() => { lbFiles.Items.Remove(item); }));
                    }
                   
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error!" + Environment.NewLine + ex.Message);
            }
            finally
            {
                Dispatcher.Invoke(new Action(() => { pBar.Visibility = System.Windows.Visibility.Collapsed; encoderHolder.IsEnabled = true; }));
            }
        }
        #endregion

        #region EventHandlers
        private void btnAdd_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Multiselect = true;
            dialog.Filter = "Images (*.BMP;*.JPG;*.JPEG;*.GIF,*.PNG,*.TIFF)|*.BMP;*.JPG;*.JPEG;*.GIF;*.PNG;*.TIFF";
            if (dialog.ShowDialog().Value)
            {
                foreach (string file in dialog.FileNames)
                {
                    lbFiles.Items.Add(file);
                }
            }

        }

        private void btnConvert_PreviewMouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            if (lbFiles.Items.Count > 0)
            {
                _sufix = tbSufix.Text;
                if (Int32.TryParse(tbWidth.Text, out _width) && Int32.TryParse(tbHeight.Text, out _height))
                {
                    encoderHolder.IsEnabled = false;
                    if (cbDefault.IsChecked.Value) _encoder = ImgEncoder.Default;
                    else if (cbBmp.IsChecked.Value) _encoder = ImgEncoder.Bmp;
                    else if (cbJpg.IsChecked.Value) _encoder = ImgEncoder.Jpg;
                    else if (cbPng.IsChecked.Value) _encoder = ImgEncoder.Png;
                    Task.Factory.StartNew(ResizeImages);
                }
            }
        }


        private void closeBtn_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            ScaleUnloaded();
        }

        private void Window_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Point pt = e.GetPosition((UIElement)sender);
                IInputElement element = InputHitTest(pt);
                if (element.GetType().Equals(typeof(System.Windows.Controls.Border))) 
                {

                    this.DragMove();
                }
            }
        }

        private void Window_PreviewKeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                List<object> list = new List<object>();
                if (lbFiles.SelectedIndex != -1)
                {
                    foreach (var itm in lbFiles.SelectedItems)
                    {
                        list.Add(itm);
                    }
                }
                foreach (var item in list)
                {
                    lbFiles.Items.Remove(item);
                }
            }
        }

        private void tbFolder_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbFolder.Text = dialog.SelectedPath;
                _folderPath = tbFolder.Text;
            }
        }

        private void tbWidth_PreviewTextInput_1(object sender, TextCompositionEventArgs e)
        {
            char c = Convert.ToChar(e.Text);
            if (Char.IsNumber(c))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void tbHeight_PreviewTextInput_1(object sender, TextCompositionEventArgs e)
        {
            char c = Convert.ToChar(e.Text);
            if (Char.IsNumber(c))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void Window_DragEnter_1(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Bitmap))
            {
                e.Effects = DragDropEffects.Copy;
            }
        }

        private void Window_Drop_1(object sender, DragEventArgs e)
        {
            string[] filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
            foreach (var path in filePaths)
            {
                switch (System.IO.Path.GetExtension(path).ToLower())
                {
                    case ".jpg":
                        lbFiles.Items.Add(path);
                        break;
                    case ".jpeg":
                        lbFiles.Items.Add(path);
                        break;
                    case ".png":
                        lbFiles.Items.Add(path);
                        break;
                    case ".bmp":
                        lbFiles.Items.Add(path);
                        break;
                    case ".gif":
                        lbFiles.Items.Add(path);
                        break;
                    case ".tif":
                        lbFiles.Items.Add(path);
                        break;
                    case ".tiff":
                        lbFiles.Items.Add(path);
                        break;
                    case ".exif":
                        lbFiles.Items.Add(path);
                        break;
                    default:
                        break;
                }
            }
        }

        private void lblAbout_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            AboutWnd about = new AboutWnd();
            about.Owner = this;
            about.Show();
        }
        #endregion


    }
}
